class Cube {
  constructor(length) {
    this.length = length;
  }

  getSideLength() {
    return this.length;
  }

  getSurfaceArea() {
    return this.length * this.length * 6;
  }

  getVolume() {
    return Math.pow(this.length, 3);
  }
}

class Triangle {
  constructor(a, b, c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  getSideLengthA() {
    return this.a;
  }

  getSideLengthB() {
    return this.b;
  }

  getSideLengthC() {
    return this.c;
  }

  getType() {
    if (this.#isEquilateral()) {
      return "Equilateral";
    } else if (this.#isIsosceles()) {
      return "Isosceles";
    } else if (this.#isRectangle()) {
      return "Rectangle";
    } else {
      return "Any";
    }
  }

  #isEquilateral() {
    return this.a === this.b && this.b === this.c && this.a == this.c;
  }

  #isIsosceles() {
    return this.a === this.b || this.a === this.c || this.b === this.c;
  }

  #isRectangle() {
    let listSizes = [this.a, this.b, this.c];
    // Search hypotenuse
    let highestSize = Math.max(...listSizes);
    // Remove hypotenuse
    listSizes.splice(listSizes.indexOf(highestSize), 1);
    // Calculate pow of the two other sizes
    let sumListSizes = listSizes.reduce(function (accumValue, curValue) {
      return Math.pow(accumValue, 2) + Math.pow(curValue, 2);
    });
    return sumListSizes === Math.pow(highestSize, 2);
  }

  getSurfaceArea() {
    return this.a * this.b * 0.5;
  }
}

module.exports = {
  Cube: Cube,
  Triangle: Triangle,
};
