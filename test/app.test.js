const Cube = require("../src/app").Cube;
const Triangle = require("../src/app").Triangle;
const expect = require("chai").expect;

describe("Testing the Cube Functions", function () {
  it("1. The side length of the Cube", function (done) {
    let c1 = new Cube(2);
    expect(c1.getSideLength()).to.equal(2);
    done();
  });

  it("2. The surface area of the Cube", function (done) {
    let c2 = new Cube(5);
    expect(c2.getSurfaceArea()).to.equal(150);
    done();
  });

  it("3. The volume of the Cube", function (done) {
    let c3 = new Cube(7);
    expect(c3.getVolume()).to.equal(343);
    done();
  });
});

describe("Testing the Triangle Functions", function () {
  it("1. The side length of the Triangle", function (done) {
    let t1 = new Triangle(2, 3, 4);
    expect(t1.getSideLengthA()).to.equal(2);
    expect(t1.getSideLengthB()).to.equal(3);
    expect(t1.getSideLengthC()).to.equal(4);
    done();
  });

  it("2. The type of the Triangle", function (done) {
    let t2 = new Triangle(2, 2, 2);
    let t3 = new Triangle(2, 4, 4);
    let t4 = new Triangle(9.6, 2.8, 10);
    let t5 = new Triangle(1, 2, 3);
    expect(t2.getType()).to.equal("Equilateral");
    expect(t3.getType()).to.equal("Isosceles");
    expect(t4.getType()).to.equal("Rectangle");
    expect(t5.getType()).to.equal("Any");
    done();
  });

  it("3. The surface area of the Triangle", function (done) {
    let t6 = new Triangle(2, 4, 5);
    expect(t6.getSurfaceArea()).to.equal(4);
    done();
  });
});
